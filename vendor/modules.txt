# cloud.google.com/go v0.97.0
## explicit
cloud.google.com/go/compute/metadata
# entgo.io/ent v0.9.1
## explicit
entgo.io/ent
entgo.io/ent/dialect
entgo.io/ent/dialect/entsql
entgo.io/ent/dialect/sql
entgo.io/ent/dialect/sql/schema
entgo.io/ent/dialect/sql/sqlgraph
entgo.io/ent/entql
entgo.io/ent/schema
entgo.io/ent/schema/edge
entgo.io/ent/schema/field
entgo.io/ent/schema/index
# github.com/AppsFlyer/go-sundheit v0.5.0
## explicit
github.com/AppsFlyer/go-sundheit
github.com/AppsFlyer/go-sundheit/checks
github.com/AppsFlyer/go-sundheit/http
# github.com/Azure/go-ntlmssp v0.0.0-20200615164410-66371956d46c
## explicit
github.com/Azure/go-ntlmssp
# github.com/Masterminds/goutils v1.1.1
## explicit
github.com/Masterminds/goutils
# github.com/Masterminds/semver v1.5.0
## explicit
github.com/Masterminds/semver
# github.com/Masterminds/semver/v3 v3.1.1
## explicit
github.com/Masterminds/semver/v3
# github.com/Masterminds/sprig/v3 v3.2.2
## explicit
github.com/Masterminds/sprig/v3
# github.com/beevik/etree v1.1.0
## explicit
github.com/beevik/etree
# github.com/beorn7/perks v1.0.1
## explicit
github.com/beorn7/perks/quantile
# github.com/cespare/xxhash/v2 v2.1.1
## explicit
github.com/cespare/xxhash/v2
# github.com/coreos/go-oidc/v3 v3.1.0
## explicit
github.com/coreos/go-oidc/v3/oidc
# github.com/coreos/go-semver v0.3.0
## explicit
github.com/coreos/go-semver/semver
# github.com/coreos/go-systemd/v22 v22.3.2
## explicit
github.com/coreos/go-systemd/v22/journal
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/dexidp/dex/api/v2 v2.0.0 => ./api/v2
## explicit
github.com/dexidp/dex/api/v2
# github.com/felixge/httpsnoop v1.0.2
## explicit
github.com/felixge/httpsnoop
# github.com/ghodss/yaml v1.0.0
## explicit
github.com/ghodss/yaml
# github.com/go-asn1-ber/asn1-ber v1.5.1
## explicit
github.com/go-asn1-ber/asn1-ber
# github.com/go-ldap/ldap/v3 v3.4.1
## explicit
github.com/go-ldap/ldap/v3
# github.com/go-sql-driver/mysql v1.6.0
## explicit
github.com/go-sql-driver/mysql
# github.com/gogo/protobuf v1.3.2
## explicit
github.com/gogo/protobuf/gogoproto
github.com/gogo/protobuf/proto
github.com/gogo/protobuf/protoc-gen-gogo/descriptor
# github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
## explicit
github.com/golang/groupcache/lru
# github.com/golang/protobuf v1.5.2
## explicit
github.com/golang/protobuf/proto
github.com/golang/protobuf/protoc-gen-go/descriptor
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/google/uuid v1.3.0
## explicit
github.com/google/uuid
# github.com/googleapis/gax-go/v2 v2.1.1
## explicit
github.com/googleapis/gax-go/v2
github.com/googleapis/gax-go/v2/apierror
github.com/googleapis/gax-go/v2/apierror/internal/proto
# github.com/gorilla/handlers v1.5.1
## explicit
github.com/gorilla/handlers
# github.com/gorilla/mux v1.8.0
## explicit
github.com/gorilla/mux
# github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
## explicit
github.com/grpc-ecosystem/go-grpc-prometheus
# github.com/huandu/xstrings v1.3.1
## explicit
github.com/huandu/xstrings
# github.com/imdario/mergo v0.3.11
## explicit
github.com/imdario/mergo
# github.com/inconshreveable/mousetrap v1.0.0
## explicit
github.com/inconshreveable/mousetrap
# github.com/jonboulle/clockwork v0.2.2
## explicit
github.com/jonboulle/clockwork
# github.com/kylelemons/godebug v1.1.0
## explicit
github.com/kylelemons/godebug/diff
github.com/kylelemons/godebug/pretty
# github.com/lib/pq v1.10.3
## explicit
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/mattermost/xml-roundtrip-validator v0.1.0
## explicit
github.com/mattermost/xml-roundtrip-validator
# github.com/mattn/go-sqlite3 v1.14.9
## explicit
github.com/mattn/go-sqlite3
# github.com/matttproud/golang_protobuf_extensions v1.0.1
## explicit
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/mitchellh/copystructure v1.0.0
## explicit
github.com/mitchellh/copystructure
# github.com/mitchellh/reflectwalk v1.0.0
## explicit
github.com/mitchellh/reflectwalk
# github.com/oklog/run v1.1.0
## explicit
github.com/oklog/run
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/prometheus/client_golang v1.11.0
## explicit
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/collectors
github.com/prometheus/client_golang/prometheus/internal
github.com/prometheus/client_golang/prometheus/promhttp
# github.com/prometheus/client_model v0.2.0
## explicit
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.26.0
## explicit
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.6.0
## explicit
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/russellhaering/goxmldsig v1.1.1
## explicit
github.com/russellhaering/goxmldsig
github.com/russellhaering/goxmldsig/etreeutils
github.com/russellhaering/goxmldsig/types
# github.com/shopspring/decimal v1.2.0
## explicit
github.com/shopspring/decimal
# github.com/sirupsen/logrus v1.8.1
## explicit
github.com/sirupsen/logrus
# github.com/spf13/cast v1.3.1
## explicit
github.com/spf13/cast
# github.com/spf13/cobra v1.2.1
## explicit
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.5
## explicit
github.com/spf13/pflag
# github.com/stretchr/testify v1.7.0
## explicit
github.com/stretchr/testify/assert
github.com/stretchr/testify/require
github.com/stretchr/testify/suite
# go.etcd.io/etcd/api/v3 v3.5.1
## explicit
go.etcd.io/etcd/api/v3/authpb
go.etcd.io/etcd/api/v3/etcdserverpb
go.etcd.io/etcd/api/v3/membershippb
go.etcd.io/etcd/api/v3/mvccpb
go.etcd.io/etcd/api/v3/v3rpc/rpctypes
go.etcd.io/etcd/api/v3/version
# go.etcd.io/etcd/client/pkg/v3 v3.5.1
## explicit
go.etcd.io/etcd/client/pkg/v3/fileutil
go.etcd.io/etcd/client/pkg/v3/logutil
go.etcd.io/etcd/client/pkg/v3/systemd
go.etcd.io/etcd/client/pkg/v3/tlsutil
go.etcd.io/etcd/client/pkg/v3/transport
go.etcd.io/etcd/client/pkg/v3/types
# go.etcd.io/etcd/client/v3 v3.5.1
## explicit
go.etcd.io/etcd/client/v3
go.etcd.io/etcd/client/v3/credentials
go.etcd.io/etcd/client/v3/internal/endpoint
go.etcd.io/etcd/client/v3/internal/resolver
go.etcd.io/etcd/client/v3/namespace
# go.opencensus.io v0.23.0
## explicit
go.opencensus.io
go.opencensus.io/internal
go.opencensus.io/internal/tagencoding
go.opencensus.io/metric/metricdata
go.opencensus.io/metric/metricproducer
go.opencensus.io/plugin/ochttp
go.opencensus.io/plugin/ochttp/propagation/b3
go.opencensus.io/resource
go.opencensus.io/stats
go.opencensus.io/stats/internal
go.opencensus.io/stats/view
go.opencensus.io/tag
go.opencensus.io/trace
go.opencensus.io/trace/internal
go.opencensus.io/trace/propagation
go.opencensus.io/trace/tracestate
# go.uber.org/atomic v1.7.0
## explicit
go.uber.org/atomic
# go.uber.org/multierr v1.6.0
## explicit
go.uber.org/multierr
# go.uber.org/zap v1.17.0
## explicit
go.uber.org/zap
go.uber.org/zap/buffer
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
go.uber.org/zap/zapcore
go.uber.org/zap/zapgrpc
# golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
## explicit
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
golang.org/x/crypto/md4
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/scrypt
# golang.org/x/net v0.0.0-20210503060351-7fd8e65b6420
## explicit
golang.org/x/net/context
golang.org/x/net/context/ctxhttp
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/timeseries
golang.org/x/net/trace
# golang.org/x/oauth2 v0.0.0-20211005180243-6b3c2da341f1
## explicit
golang.org/x/oauth2
golang.org/x/oauth2/authhandler
golang.org/x/oauth2/bitbucket
golang.org/x/oauth2/github
golang.org/x/oauth2/google
golang.org/x/oauth2/google/internal/externalaccount
golang.org/x/oauth2/internal
golang.org/x/oauth2/jws
golang.org/x/oauth2/jwt
# golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac
## explicit
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.6
## explicit
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# google.golang.org/api v0.59.0
## explicit
google.golang.org/api/admin/directory/v1
google.golang.org/api/googleapi
google.golang.org/api/googleapi/transport
google.golang.org/api/internal
google.golang.org/api/internal/gensupport
google.golang.org/api/internal/impersonate
google.golang.org/api/internal/third_party/uritemplates
google.golang.org/api/option
google.golang.org/api/option/internaloption
google.golang.org/api/transport/cert
google.golang.org/api/transport/http
google.golang.org/api/transport/http/internal/propagation
google.golang.org/api/transport/internal/dca
# google.golang.org/appengine v1.6.7
## explicit
google.golang.org/appengine
google.golang.org/appengine/internal
google.golang.org/appengine/internal/app_identity
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/modules
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
# google.golang.org/genproto v0.0.0-20211008145708-270636b82663
## explicit
google.golang.org/genproto/googleapis/api/annotations
google.golang.org/genproto/googleapis/rpc/code
google.golang.org/genproto/googleapis/rpc/errdetails
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.41.0
## explicit
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/internal/xds/env
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/reflection
google.golang.org/grpc/reflection/grpc_reflection_v1alpha
google.golang.org/grpc/resolver
google.golang.org/grpc/resolver/manual
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.27.1
## explicit
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/square/go-jose.v2 v2.6.0
## explicit
gopkg.in/square/go-jose.v2
gopkg.in/square/go-jose.v2/cipher
gopkg.in/square/go-jose.v2/json
# gopkg.in/yaml.v2 v2.4.0
## explicit
gopkg.in/yaml.v2
# gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
## explicit
gopkg.in/yaml.v3
# github.com/dexidp/dex/api/v2 => ./api/v2
